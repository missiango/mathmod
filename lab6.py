﻿import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as od

def wars(n1, b1, n2, b2):
    N1 = []
    N2 = []
    a1 = 0
    a2 = 0
    y1 = 0
    y2 = 0
    n1x = n1
    n2x = n2
    #c=b1*n2 ** 2 - b2*n2 ** 2
    z=1
    i = 0
    while(i<5000):
        dn1=(-a1*n1x-b2*n2x+y1)*0.1;
        dn2=(-a2*n2x-b1*n1x+y2)*0.1;
        n1x=n1x+dn1
        n2x=n2x+dn2
        if n2x < 0:
            break
        if n1x < 0:
            break
        N1.append(n1x)
        N2.append(n2x)
        i+=1
    return [N1, N2]

def newwars(n1, b1, a1, n2, b2, a2, a):
    N1 = []
    N2 = []
    y1 = 0
    y2 = 0
    n1x = n1
    n2x = n2
    #c=b1*n2 ** 2 - b2*n2 ** 2
    z=1
    i = 0
    t = a
    while(i<50000):
        dn1=(-a1*n1x-a*(n2x-n1x)-b2*n2x+y1)*0.1
        dn2=(-a2*n2x-t*(n1x-n2x)-b1*n1x+y2)*0.1
        n1x=n1x+dn1
        n2x=n2x+dn2
        if n2x < 0:
            break
        if n1x < 0:
            break
        N1.append(n1x)
        N2.append(n2x)
        i+=1
        t+=0.00001
    return [N1, N2]

N11 = 200
N22 = 150
b1 = 0.01
b2 = 0.01
ret = wars(N11,b1, N22, b2)
N2 = ret.pop()
N1 = ret.pop()
plt.figure()
plt.hold(True)
plt.grid()
plots = []
plots.append(plt.plot(N1, N2,'b')[0])

i = 10
while(i < 50):
    ret = wars(N11, b1, N22 + i, b2)
    y = ret.pop()
    x = ret.pop()
    plots.append(plt.plot(y,x,'g')[0])
    ret = wars(N11 + i, b1, N22, b2)
    y = ret.pop()
    x = ret.pop()
    plots.append(plt.plot(x,y, 'r'))
    i+=10
plt.show()
#plt.figure()
#plt.hold(True)
#plots.clear()
#plots.append(plt.plot(N1, N2,'b')[0])
#i = 0.1
#ret = wars(N11, b1, N22, b2 + i)
#y = ret.pop()
#x = ret.pop()
#plots.append(plt.plot(x,y,'g')[0])
#ret = wars(N11, b1+i, N22, b2)
#y = ret.pop()
#x = ret.pop()
#plots.append(plt.plot(x,y,'r')[0])
#plt.show()
N1=2000
N2=2000
b1=0.001
b2=0.001
a=0.0025
a1=0.001
a2=0.001
plots.clear()
plt.figure()
plt.hold(True)
plt.grid(True)
ret = newwars(N1,b1,a1,N2,b2,a2,a)
y = ret.pop()
x = ret.pop()
plots.append(plt.plot(x, y,'b')[0])
i = 100
while( i < 500):
    ret = newwars(N1,b1,a1,N2+i,b2,a2,a)
    y = ret.pop()
    x = ret.pop()
    plots.append(plt.plot(x, y,'g')[0])
    ret = newwars(N1+i,b1,a1,N2,b2,a2,a)
    y = ret.pop()
    x = ret.pop()
    plots.append(plt.plot(x, y,'r')[0])
    i+=100
plt.show()

