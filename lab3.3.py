import numpy
import matplotlib.pyplot as plt
import scipy.integrate

def fun(y, t):
    #Second part
    f = [-np.sin(t), -np.cos(t)]
    return f

init = 1.0, 0.0
t = np.linspace(0, 10, 100)
y = odeint(fun, init, t)
plt.plot(t, np.cos(t), 'r+', t, y) 
