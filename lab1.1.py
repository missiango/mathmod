__author__ = 'Slava'
import numpy as np
import matplotlib.pyplot as plt
v = []
m = []
t = []
dt=0.1
dm=50
mp=0
m1=700000
m0=m1+mp
v0=0
m.append(0)
v.append(0)
t.append(0)
myLambda=0.1
u=3000
i=0
while(m[i]<m0*(1-myLambda)):
    t.append(t[i]+dt)
    m.append(m[i]+dm)
    v.append(v0 + u*np.log(m0/(m0-m[i+1])))
    i=i+1
plt.plot(t,v)
plt.grid()
plt.xlabel("t")
plt.ylabel("v")
plt.show()



