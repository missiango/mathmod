import numpy
import matplotlib.pyplot as plt
import scipy.integrate

def fun(t, y):
    #Second part test
    y1 = 1/(1 + y ** 2)
    y2 = 2*y/(1 + y ** 2)
    return y1, y2

init = 0.0, 1.0
t = np.linspace(0, 10, 100)
y = odeint(fun, init, t)
plt.plot(t, np.arctan(t), 'r+', t, y) 
