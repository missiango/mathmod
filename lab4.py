import numpy
import matplotlib.pyplot as plt
import scipy.integrate

def malt(t, y):
    alpha = 0.3
    betta = 0.1
    malt = (alpha - betta)*y
    return malt

y0 = 100
t = np.linspace(0, 100)
y = odeint(malt, y0, t)
plt.plot(t, y)
plt.grid()
plt.show()
