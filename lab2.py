import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos, pi
import random
import math
 
deviation = 0.003
rocket_initial_speed = 1000.0
alpha = pi / 4.0
g = 9.81
flight_time = (2 * rocket_initial_speed * sin(alpha) / g)
x = []
y = []
timeline = np.arange(0, math.ceil(flight_time), 1)
for t in timeline:
    x.append(rocket_initial_speed * t * cos(alpha))
    y.append(rocket_initial_speed * t * sin(alpha) - g * t ** 2 / 2)

plt.plot(x, y, 'g-')
plt.grid()
plt.show()

xe = x[14:40]
ye = []
for i in range(0, 26):
    ye.append(y[i + 14] * (1 + np.random.rand() * deviation))

xe = np.transpose(xe)
ye = np.transpose(ye)

A_matrix_source = []

for xx in xe:
    A_matrix_source.append([xx ** 2, xx, 1.0])
A = np.matrix(A_matrix_source)
AA = np.transpose(A) * A

matrix_conditional_number = np.linalg.cond(AA)

koef = np.linalg.inv(AA) * np.transpose(A) * ye.reshape(26, 1)
yet = A * koef
plt.grid()
plt.plot(xe, ye, '+', x[14:40], y[14:40], x[14:40], yet)
plt.show()

reconstructed_rocket_trajectory = [[xi ** 2, xi, 1.0] for xi in x] * koef

plt.plot(x, y, 'r-')
plt.hold()
plt.plot(x, reconstructed_rocket_trajectory, 'g+')
plt.grid()
plt.show()
matrix_to_list = lambda matrix: [i[0] for i in matrix.tolist()]
koef_as_list = matrix_to_list(koef)

vch = math.sqrt(2 * g * (koef_as_list[2] - koef_as_list[1] ** 2 / (4 * koef_as_list[0])))
vzn = sin(math.atan(koef_as_list[1] ** 2 - 4 * koef_as_list[0] * koef_as_list[2]))
antirocket_rocket_initial_speed = vch / vzn
v0x = antirocket_rocket_initial_speed * cos(alpha)
v0y = antirocket_rocket_initial_speed * sin(alpha)
L = flight_time * rocket_initial_speed * cos(alpha)
L1 = 0.75 * L
u0 = 2000.0
t0 = 39.0

a = (v0x * u0 * t0) - (L1 * u0)
b = (v0y * u0 * t0) - ((g * u0 * (t0 ** 2)) / 2)
c = (L1 * g * t0) - (L1 * v0y) - ((g * v0x * (t0 ** 2)) / 2)

antirocket_theta = math.asin(a / math.sqrt(a ** 2 + b ** 2))  
bb = antirocket_theta + math.acos(c / math.sqrt(a ** 2 + b ** 2))
terminate_time = (L1 - v0x * t0) / (v0x + u0 * math.cos(bb))
u0x = u0 * math.cos(bb)
u0y = u0 * sin(bb)

L2 = (v0x * flight_time) * 0.75
gama = (math.asin((L2 * g / (u0 ** 2)))) / 2
tp = 2 * u0 * sin(gama) / g

v1 = 1000.0
v1x = v1 * cos(alpha)
v1y = v1 * sin(alpha)

x_anti_rocket = [L1 - u0x * (t - t0) for t in np.arange(t0, t0 + terminate_time, 0.1)]
y_anti_rocket = [u0y * (t - t0) - ((g * (t - t0) ** 2) / 2) for t in np.arange(t0, t0 + terminate_time, 0.1)]

plt.clf()
plt.plot(x_anti_rocket, y_anti_rocket, 'b.')
plt.hold()

y_revenge_rocket = [u0 * sin(gama) * (t - t0) - ((g * (t - t0) ** 2) / 2) for t in np.arange(t0, t0 + tp, 0.1)]
x_revenge_rocket = [L1 - u0 * cos(gama) * (t-t0)  for t in np.arange(t0, t0 + tp, 0.1)]
plt.plot(x_revenge_rocket, y_revenge_rocket, 'g.')
br_index = int(math.ceil(t0 + terminate_time))

plt.plot(x[:br_index], y[:br_index], 'r-') 
plt.plot(x[:br_index], reconstructed_rocket_trajectory[:br_index], 'g+')
plt.show()
