import os
from math import sin, cos, pi
import matplotlib.pyplot as plt
from scipy.integrate import ode
import numpy as np

def fun(t: float, army_strength: np.array) -> np.array:
    if army_strength[0] < 0 or army_strength[1] < 0:
        return [0.0, 0.0]
    if( t > 1.0 and t < 1.1):
        return [-non_military_death_factor_c1 * koef_c1 * army_strength[0] - military_death_factor_c1 * koef_c1 * army_strength[
            1] + 10,

                -non_military_death_factor_c2 * koef_c2 * army_strength[1] - military_death_factor_c2 * koef_c2 * army_strength[0] *
                army_strength[1] - 10]
    else:
        return [-non_military_death_factor_c1 * koef_c1 * army_strength[0] - military_death_factor_c1 * koef_c1 * army_strength[
            1] + reinforcement_speed_c1,

                -non_military_death_factor_c2 * koef_c2 * army_strength[1] - military_death_factor_c2 * koef_c2 * army_strength[0] *
                army_strength[1] + reinforcement_speed_c2]

def integrate(differential_equation, t_start: float, t_final: float, initial_condition):
    step = 0.05
    results = list()
    x = list()

    solver = ode(differential_equation).set_integrator('dopri5')
    solver.set_initial_value(initial_condition, t_start)

    eps = 0.0000001
    while solver.successful() and solver.t < t_final - eps:
        solver.integrate(solver.t + step)
        x.append(solver.t)
        results.append(solver.y)

    return x, results


if __name__ == '__main__':
    non_military_death_factor_c1 = 0.3
    non_military_death_factor_c2 = 0.3

    koef_c1 = 0.9
    koef_c2 = 0.5

    military_death_factor_c1 = 0.4
    military_death_factor_c2 = 0.05

    reinforcement_speed_c1 = 0
    reinforcement_speed_c2 = 0

    end_time = 10.0
    samples_count = 10

    M10 = M20 = 100

    plt.axis([0, M10, 0, M20])
    plt.hold(True)

    for i in range(0, samples_count):
        M0 = cos(pi / 2.0 * i / samples_count) * M10
        N0 = sin(pi / 2.0 * i / samples_count) * M20

        [t, Y] = integrate(fun, 0.0, end_time, [M0, N0])
        m1 = np.array(Y)[:, [0]]
        m2 = np.array(Y)[:, [1]]
        plt.plot(m1, m2)

    plt.xlabel('Army')
    plt.ylabel('Partisan')
    plt.grid(True)
    plt.show()
