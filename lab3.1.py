import numpy
import matplotlib.pyplot as plt
import scipy.integrate

def fun(y, t):
    #Method 1
    f = 2 * t
    return f

#Method 1
y0 = 0.0
t = np.linspace(0, 1, 51)
y = odeint(fun, y0, t)
plt.plot(t,t ** 2,'g+',t,y[:,0],'r')
plt.grid()
#plt.plot(t, y[:,0])
plt.xlabel('t')
plt.ylabel('y')
plt.show()
