import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as od

def model(t,y):
    alpha_betta = [[0.4, 0.1], [0.02, 0.01]]
    ret = np.zeros((2,1))
    gamma = 0.01 
    alpha_betta[0][0] *= (1 + gamma*np.sin(t))
    alpha_betta[0][1] *= (1 + gamma*np.sin(t))
    ret[0] = ( alpha_betta[0][0] - alpha_betta[1][0] * y[1]) * y[0]
    ret[1] = (-alpha_betta[0][1] + alpha_betta[1][1] * y[0]) * y[1]
    return ret


n0 = 50;
m0 = 10;
T = 100
dt = T/200.
solver = od.ode(model).set_integrator('zvode', method='bdf')
y0 = [n0, m0]
solver.set_initial_value(y0)

t = []
y = []

while solver.successful() and solver.t <= T:
    solver.integrate(T, step = True)
    y.append(solver.y)
    t.append(solver.t)
plt.plot(t, y)
plt.xlabel('t (Time)')
plt.ylabel('N (Count)')
plt.title('Count of victims and predators')
plt.grid()
plt.show()

plt.plot(t, (np.array(y)[:,[0]] / np.array(y)[:,[1]]))
plt.xlabel('t (time)')
plt.ylabel('Otn ( Count of victims / Count of predators )')
plt.title('Otn Population par time')
plt.grid()
plt.show()

plt.plot(np.array(y)[:,[0]], np.array(y)[:,[1]])
plt.xlabel('N (Count of victims)')
plt.ylabel('M (Count of predators)')
plt.title('Fas. portret')
plt.grid()
plt.show();


   
