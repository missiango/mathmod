__author__ = 'Slava'
import numpy as np
import matplotlib.pyplot as plt

m = []
v = []
t = []
dt=0.1
dm=25
mp=50
m1=1000000
m2=500000
m3=300000
m0=mp+m1+m2+m3
v0=0
m.append(0)
v.append(0)
t.append(0)
myLambda=0.1
u=3000
i=0

while(m[i]<m1*(1-myLambda)):
    t.append(t[i]+dt)
    m.append(m[i]+dm)
    v.append(v0 + u*np.log(m0/(m0-m[i+1])))
    i=i+1
plt.plot(t,v)
v0 = v[i-1]
m.clear()
m.append(0)
temp = t[i-1] + dt
t.clear()
t.append(temp)
v.clear()
v.append(v0)
i=0
m0=mp+m2+m3

while(m[i]<m2*(1-myLambda)):
    t.append(t[i]+dt)
    m.append(m[i]+dm)
    v.append(v0 + u*np.log(m0/(m0-m[i+1])))
    i=i+1
plt.plot(t,v)
v0 = v[i-1]
m.clear()
m.append(0)
temp = t[i-1] + dt
t.clear()
t.append(temp)
v.clear()
v.append(v0)
i=0
m0=mp+m3

while(m[i]<m3*(1-myLambda)):
    t.append(t[i]+dt)
    m.append(m[i]+dm)
    v.append(v0 + u*np.log(m0/(m0-m[i+1])))
    i=i+1
plt.plot(t,v)
plt.grid()
plt.xlabel("t")
plt.ylabel("v")
plt.show()

